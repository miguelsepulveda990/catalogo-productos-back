from flask import Flask, jsonify, request
from products import products
from flask_cors import CORS

app = Flask(__name__)
CORS(app)


@app.route('/getProducts', methods=["GET"])
def getProducts():
    return jsonify(products)


@app.route('/getByName/<string:product_name>', methods=["GET"])
def getByName(product_name):
    find = [product for product in products if product['name'] == product_name]
    if (len(find) > 0):
        return jsonify({'products': find})
    return jsonify({'message': 'Product no found'})


@app.route('/getById/<string:product_id>', methods=["GET"])
def getById(product_id):
    find = [product for product in products if product['sku'] == product_id]
    if (len(find) > 0):
        return jsonify({'products': find})
    return jsonify({'message': 'Product id no found'})


@app.route('/addProduct', methods=["POST"])
def addProduct():
    find = [product for product in products if product['sku'] == request.json['sku']]

    if(len(find) > 0):
        return jsonify({'message': 'product already exists'})
    new_product = {
        "sku": request.json['sku'],
        "name":  request.json['name'],
        "price":  request.json['price'],
        "currency":  request.json['currency']
    }
    products.append(new_product)
    return jsonify({'message': 'success'})


@app.route('/removeProduct/<string:product_id>', methods=["DELETE"])
def removeProduct(product_id):
    find = [product for product in products if product['sku'] == product_id]
    if (len(find) > 0):
        products.remove(find[0])
        return jsonify({'mesagge': 'success'})
    return jsonify({'message': 'unsuccess'})


@app.route('/updateProduct', methods=["PUT"])
def updateProduct():
    find = [product for product in products if product['sku']
            == request.json['sku']]
    if (len(find) > 0):
        find[0]['name'] = request.json['name']
        find[0]['price'] = request.json['price']
        find[0]['currency'] = request.json['currency']
        return jsonify({'message': 'success'})

    return jsonify({'message': 'unsuccess'})


if __name__ == '__main__':
    app.run(debug=True, port=4000)
