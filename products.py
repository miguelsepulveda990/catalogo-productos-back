products = [
    {
        "sku": "110011001100",
        "name": "Televisor",
        "price": "150",
        "currency": "USD"
    },
    {
        "sku": "220022002200",
        "name": "Computador",
        "price": "200",
        "currency": "USD"
    },
    {
        "sku": "330033003300",
        "name": "Proyector",
        "price": "300",
        "currency": "USD"
    },
    {
        "sku": "440044004400",
        "name": "Celular",
        "price": "250",
        "currency": "USD"
    },
    {
        "sku": "550055005500",
        "name": "Televisor",
        "price": "300",
        "currency": "USD"
    },
]
